import math

rm = 71

def isPrime(n):
    if n < 2:
        return False

    for i in range(2, math.ceil(math.sqrt(n)) + 1):
        if n % i == 0:
            return False

    return True

def generateBases(bitLength, baseLength):
    base1 = []
    base2 = []
    x = 2 ** bitLength
    for i in range(0, baseLength):
        x -= 1
        while not isPrime(x):
            x -= 1
        base2.append(x)
    for i in range(0, baseLength):
        x -= 1
        while not isPrime(x):
            x -= 1
        base1.append(x)
    return (base1, base2)

bitLength = 31
baseLength = 52
base1, base2 = generateBases(bitLength, baseLength)

def printVectForOCaml(v):
    print("[|", end = "")
    for i in range(0, baseLength):
        print(v[i], end = "")
        if i != baseLength - 1:
            print(";", end = " ")
    print("|]", end = "")

def printForOCaml(v1, v2):
    print("(", end = "")
    printVectForOCaml(v1)
    print(",", end = " ")
    printVectForOCaml(v2)
    print(")")

printVectForOCaml(base1)
print()
printVectForOCaml(base2)
print()

from Crypto.PublicKey import RSA
from math import *
RSAkey = RSA.generate(1024)
print("N")
print(getattr(RSAkey.key, 'n'))
print("\n\nE")
print(getattr(RSAkey.key, 'e'))
print("\n\nD")
print(getattr(RSAkey.key, 'd'))

N = getattr(RSAkey.key, 'n')
aux = 1
test = 0
for i in range(0, baseLength):
	aux = (aux * base1[i]) % N
	test += log(base1[i], 2)
print("\n\n")
print(log(N,2))
print("\n\n")
print(test)
aux = (aux * aux) % N
print("\n\nM1SQ mod N")
print(aux)


