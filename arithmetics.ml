let rec extendedEuclid a b =
    if b = 0 then
        (1, 0, a)
    else
        let (x1, y1, d) = extendedEuclid b (a mod b) in
            (y1, x1 - (a / b) * y1, d)

let invMod a b = 
    let (x, _, _) = extendedEuclid a b in
        if x > 0 then
            x mod b
        else
            b + x mod b
