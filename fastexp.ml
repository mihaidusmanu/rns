open Montgomery
open Constants
open Bases
open Operators

(* Return nb ^ pow mod n and redundant modulus in both RNS bases *)
let logPow nb nbrm pow n nrm m1sq m1sqrm = 
    let (a1,a2) = rnsMM (zero, m12) m1rm (zero, m12) m1rm n nrm in
    let ans = ref a1 in
    let ansrm = ref a2 in
    let (b1, b2) = rnsMM nb nbrm m1sq m1sqrm n nrm in
    let aux = ref b1 in
    let auxrm = ref b2 in
    let rec fct pow = 
        match pow with
        | [] -> ()
        | x :: powp -> (
            if x = 1 then 
                let (x1, x2) = rnsMM !ans !ansrm !aux !auxrm n nrm in (
                    ans := x1;
                    ansrm := x2);
            let (y1, y2) = rnsMM !aux !auxrm !aux !auxrm n nrm in (
                aux := y1;
                auxrm := y2;
                fct powp)) in 
        fct pow;
        rnsMM !ans !ansrm (un, un) unrm n nrm 
