open Constants
open Arithmetics

(* Returns (a + b) mod c *)
let addMod a b c = 
    let ans = a + b in
        if ans >= c then
            ans - c
        else
            ans

(* Returns (a * b) mod c *)
let prodMod a b c =
    (a * b) mod c

(* Returns -a mod c *)
let minusMod a c = 
    if a = 0 then
        0
    else
        c - a

(* Returns a + b in first RNS base *)
let (@+) a b = 
    let ans = Array.make baseLength 0 in
        for i = 0 to baseLength - 1 do
            ans.(i) <- addMod a.(i) b.(i) base1.(i)
        done;
        ans

(* Returns a + b in second RNS base *)
let (@+.) a b =
    let ans = Array.make baseLength 0 in
        for i = 0 to baseLength - 1 do
            ans.(i) <- addMod a.(i) b.(i) base2.(i)
        done;
        ans

(* Returns a * b in first RNS base *)
let (@*) a b =
    let ans = Array.make baseLength 0 in
        for i = 0 to baseLength - 1 do
            ans.(i) <- prodMod a.(i) b.(i) base1.(i)
        done;
        ans

(* Returns a * b in second RNS base *)
let (@*.) a b =
    let ans = Array.make baseLength 0 in
        for i = 0 to baseLength - 1 do
            ans.(i) <- prodMod a.(i) b.(i) base2.(i)
        done;
        ans

(* Returns a ^ -1 in first RNS base *)
let (~^) a = 
    let inva = Array.make baseLength 0 in
        for i = 0 to baseLength - 1 do
            inva.(i) <- invMod a.(i) base1.(i)
        done;
        inva

(* Returns a ^ -1 in second RNS base *)
let (~^.) a = 
    let inva = Array.make baseLength 0 in
        for i = 0 to baseLength - 1 do
            inva.(i) <- invMod a.(i) base2.(i)
        done;
        inva

(* Returns -a in first RNS base *)
let (~-) a = 
    let ma = Array.make baseLength 0 in
        for i = 0 to baseLength - 1 do
            ma.(i) <- minusMod a.(i) base1.(i)
        done;
        ma

(* Returns -a in second RNS base *)
let (~-.) a = 
    let ma = Array.make baseLength 0 in
        for i = 0 to baseLength - 1 do
            ma.(i) <- minusMod a.(i) base2.(i)
        done;
        ma

(* Tests if 2 numbers in the same RNS base are equal *)
let (@=) a b =
    let eq = ref true in
        for i = 0 to baseLength - 1 do
            if a.(i) <> b.(i) then
                eq := false
        done;
        !eq

