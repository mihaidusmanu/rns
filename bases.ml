open Constants
open Operators
open Arithmetics

let precomputeBase base1 base2 = 
    let mk1 = Array.make baseLength 0 in
    let invmk1 = Array.make baseLength 0 in
    let mk2 = Array.make_matrix baseLength baseLength 0 in
    let invmk2 = Array.make_matrix baseLength baseLength 0 in
        for i = 0 to baseLength - 1 do
            let aux = ref 1 in
                for j = 0 to baseLength - 1 do
                    if j <> i then
                        aux := prodMod (!aux) base1.(j) base1.(i)
                done;
                mk1.(i) <- !aux;
                invmk1.(i) <- invMod !aux base1.(i)
        done;
        for i = 0 to baseLength - 1 do
            for j = 0 to baseLength - 1 do
                let aux = ref 1 in
                    for k = 0 to baseLength - 1 do
                        if k <> i then
                            aux := prodMod !aux base1.(k) base2.(j)
                    done;
                    mk2.(i).(j) <- !aux;
                    invmk2.(i).(j) <- invMod !aux base2.(j)
            done;
        done;
        (mk1, invmk1, mk2, invmk2)

let (m1k1, invm1k1, m1k2, invm1k2) = precomputeBase base1 base2

let (m2k2, invm2k2, m2k1, invm2k1) = precomputeBase base2 base1

let mChangeBase base1 base2 =
    let m = Array.make baseLength 1 in
    let invm = Array.make baseLength 0 in
        for i = 0 to baseLength - 1 do
            for j = 0 to baseLength - 1 do
                m.(i) <- prodMod m.(i) (base1.(j) mod base2.(i)) base2.(i)
            done;
            invm.(i) <- invMod m.(i) base2.(i)
        done;
        (m, invm)

let (m12, invm12) = mChangeBase base1 base2

let (m21, invm21) = mChangeBase base2 base1

let mRedundantModulus base = 
    let mrm = ref 1 in
        for i = 0 to baseLength - 1 do 
            mrm := (!mrm * (base.(i) mod rm)) mod rm
        done;
        let mkrm = Array.make baseLength 0 in
            for i = 0 to baseLength - 1 do
                mkrm.(i) <- (!mrm * (invMod (base.(i) mod rm) rm)) mod rm
            done;
            ((!mrm), invMod (!mrm) rm, mkrm)

let (m1rm, invm1rm, m1krm) = mRedundantModulus base1

let (m2rm, invm2rm, m2krm) = mRedundantModulus base2
