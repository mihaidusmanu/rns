open Encode
open Decode
open Rsakey
open Operators
open Constants

let (messCode, messCoderm) = encrypt
let ((finCode1, finCode2), _) = decrypt messCode messCoderm

let _ =
    let (n1, n2) = n in
    let (pass1, pass2) = pass in
    let aux = ref finCode1 in
    let ans = ref false in 
        for i = 0 to baseLength + 3 do
            if !aux @= pass1 then (
                ans := true);
            aux := !aux @+ (~- n1)
        done;
        if !ans = true then
            Printf.printf "Password accepted\n"
        else
            Printf.printf "Wrong password\n"
