open Bigint 
open Pconstants
open Fastexp
open Constants
open Conversions

let messdec = bigIntFromFile "./message.txt"
let mess = (decToRNS messdec base1, decToRNS messdec base2)
let messrm = modInt messdec rm

let encrypt = logPow mess messrm publicKeybin n nrm m1sq m1sqrm
