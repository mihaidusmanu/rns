open Bigint

let initialpass = bigIntFromFile "./input/password.txt"
let file = "message.txt"
let oc = open_out file

let rec parc p = 
    match p with
    | [] -> 1
    | x :: pp -> 
        let m = Random.int(150) in
        let r = parc pp in
            if m < 149 then
                Printf.fprintf oc "%d" x
            else
                Printf.fprintf oc "%d" (Random.int(9) + 1);
            r

let _ = Random.self_init ()
let _ = parc initialpass
let _ = close_out oc
        
