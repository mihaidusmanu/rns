open Bigint
open Constants

let decToRNS nb base = 
    let rnb = Array.make baseLength 0 in
        for i = 0 to baseLength - 1 do
            rnb.(i) <- modInt nb base.(i)
        done;
        rnb

let rec decToBin nb = 
    match nb with
    | [] -> []
    | lastDigit :: _ -> (lastDigit mod 2) :: (decToBin (div2 nb))
