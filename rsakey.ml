open Montgomery
open Bigint
open Conversions
open Constants

let ndec = bigIntFromFile "./input/n.txt"
let n =  (decToRNS ndec base1, decToRNS ndec base2)
let nrm = modInt ndec rm

let publicKeydec = bigIntFromFile "./input/publickey.txt"
let publicKeybin = decToBin publicKeydec

let privateKeydec = bigIntFromFile "./input/privatekey.txt"
let privateKeybin = decToBin privateKeydec

let m1sqdec = bigIntFromFile "./input/m1sq.txt"
let m1sq = (decToRNS m1sqdec base1, decToRNS m1sqdec base2)
let m1sqrm = modInt m1sqdec rm

let passdec = bigIntFromFile "./input/password.txt"
let pass = (decToRNS passdec base1, decToRNS passdec base2)
let passrm = modInt passdec rm
