let base = 10

(* Returns a big integer that was read from file *)
let bigIntFromFile file = 
    let readFile = open_in file in
    let line = input_line readFile in
    let len = String.length line in
    let nb = ref [] in
    let rec aux pos = 
        if pos < len then (
            nb := (int_of_char line.[pos] - int_of_char '0') :: !nb;
            aux (pos + 1))
    in aux 0;
    !nb

(* Returns the quotient of nb / 2  *)
let div2 nb =
    let rec aux nb =  
        match nb with
        | [] -> ([], 0)
        | x :: nbp -> let (q, r) = aux nbp in (
            if q <> [] then
                (((x + r * base) / 2) :: q, (x + r * base) mod 2)
            else if (x + r * base) / 2 = 0 then
                ([], (x + r * base) mod 2)
            else
                ([(x + r * base) / 2], (x + r * base) mod 2)) in
    let (ans, _) = aux nb in
        ans

(* Returns nb mod v where v is int *)
let rec modInt nb v = 
    match nb with
    | [] -> 0
    | x :: nbp -> (modInt nbp v * 10 + x) mod v 
