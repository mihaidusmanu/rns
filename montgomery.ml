open Constants
open Bases
open Operators

let firstBaseExtension q1 =
    let sigma = Array.make baseLength 0 in
        for i = 0 to baseLength - 1 do
            sigma.(i) <- prodMod q1.(i) invm1k1.(i) base1.(i)
        done;
        let q2 = Array.make baseLength 0 in
        let modrm = ref 0 in
            for j = 0 to baseLength - 1 do
                for i = 0 to baseLength - 1 do
                    q2.(j) <- addMod q2.(j) (prodMod m1k2.(i).(j) sigma.(i) base2.(j)) base2.(j)
                done;
                modrm := (!modrm + (m1krm.(j) * sigma.(j)) mod rm) mod rm
            done;
            (q2, !modrm)

let computeBeta sigma modrm = 
    let aux = ref 0 in
        for j = 0 to baseLength - 1 do
            aux := (!aux + (m2krm.(j) * sigma.(j)) mod rm) mod rm
        done;
        aux := (!aux + (rm - modrm)) mod rm;
        (invm2rm * !aux) mod rm

let secondBaseExtension q2 modrm =
    let sigma = Array.make baseLength 0 in
        for i = 0 to baseLength - 1 do
            sigma.(i) <- prodMod q2.(i) invm2k2.(i) base2.(i)
        done;
        let beta = computeBeta sigma modrm in
        let q1 = Array.make baseLength 0 in
            for i = 0 to baseLength - 1 do
                for j = 0 to baseLength - 1 do
                    q1.(i) <- addMod q1.(i) (prodMod m2k1.(j).(i) sigma.(j) base1.(i)) base1.(i)
                done;
                q1.(i) <- addMod q1.(i) (minusMod (prodMod beta m21.(i) base1.(i)) base1.(i)) base1.(i)
            done;
            q1

(* Return a * b * (m ^ (-1)) mod n in both RNS bases *)
let rnsMM a arm b brm phi phirm = 
    let (a1, a2) = a in
    let (b1, b2) = b in
    let (phi1, phi2) = phi in
    let miphi1 = ~- (~^ phi1) in
    let q1 = ((a1 @* b1) @* miphi1) in
    let (q2, modrm) = firstBaseExtension q1 in
    let r2 = (((a2 @*. b2) @+. (q2 @*. phi2)) @*. invm12) in
    let newmodrm = (((((arm * brm) mod rm) + ((modrm * phirm) mod rm)) mod rm) * invm1rm) mod rm in
    let r1 = secondBaseExtension r2 newmodrm in
        ((r1, r2), newmodrm)
